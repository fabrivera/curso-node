"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// funcion con nombre o nombrada
function suma(x, y) {
    return x + y;
}
// funcion flecha
const sumaFlecha = (x, y) => {
    return x + y;
};
// console.log(suma(1,2) );
// funcion con parametros opcionales
const funcionOpcionalSuma = (x, y) => {
    if (!y)
        return x;
    return x + y;
};
// console.log(funcionOpcionalSuma(1));
// console.log(funcionOpcionalSuma(1,2));
// Otra opcion es agregarle un valor por defecto
const funcionOpcionalSuma2 = (x, y = 0) => {
    return x + y;
};
// division entre 2 parametros anulando la división por 0
// Devuelve any porque puede ser un numero o string
const funcionDivision = (dividendo, divisor) => {
    if (divisor == 0) {
        return '\x1b[33mno se puede dividir por 0\x1b[0m';
    }
    //Otra posibilidad es return dividendo/0 = Infinity (number)
    return dividendo / divisor;
};
const potencia = (base, exponente) => {
    let resultado = 1;
    for (let cuenta = 0; cuenta < exponente; cuenta++) {
        resultado *= base;
    }
    return resultado;
};
console.log('Suma opcional 1: ', funcionOpcionalSuma2(1));
console.log('Suma de 1 y 2: ', funcionOpcionalSuma2(1, 2));
console.log('División de 27 en 3: ', funcionDivision(27, 3));
console.log('División de 10 en 0: ', funcionDivision(10, 0));
console.log('Potencia de base 2 con exp 10: ', potencia(2, 10));
