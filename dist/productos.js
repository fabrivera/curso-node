"use strict";
//
const Producto1 = {
    nombre: "Yogurt",
    precio: 100,
    stock: 13
};
const Producto2 = {
    nombre: "Paquete de Galletas",
    precio: 89.9,
    stock: 40
};
const Producto3 = {
    nombre: "Lata Coca-Cola",
    precio: 80.50,
    stock: 0
};
const Producto4 = {
    nombre: "Alfajor",
    stock: 12,
    precio: 40
};
let productos = [
    Producto1,
    Producto2,
    Producto3,
    Producto4
];
console.log(productos);
function getStock(productos) {
    for (let i = 0; i < productos.length; i++) {
        if (productos[i].stock === 0) {
            console.log(`Lo sentimos, no hay mas unidades del Producto ${productos[i].nombre}`);
        }
        else {
            console.log(`Del Producto ${productos[i].nombre} quedan ${productos[i].stock} unidades`);
        }
    }
}
getStock(productos);
